console.log("Hello Cami")

// function message(){
// 	console.log("'Di lang kakayanin, kayang-kaya!")
// }

// message()

function showMessage(){
	console.log("Don't text her back.");
}

/*showMessage()
showMessage()
showMessage()
showMessage()
showMessage()*/

//while loop
//allows us to repeat a task/code while the condition is true

/*
	while(condition){
	
		task;
		increment/decrement;

	}
*/

//varianle for counter, it will set how many times we will be able to repeat our task

let count = 5;

//as long as our count variable is not equal to zero, we will be able to repear our task
/*while(count!==0){
	//as long as si count ay hindi magzero,di magstop

	showMessage();
	//if a loop is not able to falsify its condition, you will be facing an infinite loop.
	count--;
}
*/
/*while(count !==0){

	console.log(count);
	count--; //way to make the condition not infinite
	//ingat sa loops
}
*/
//first time loop run: count=5 -> before we start the next loop, we decrement to 4;
//second time loop run: count=4 -> before we start the next loop, we decrement to 3;
//third time loop run: count=3 -> before we start the next loop, we decrement to 2;
//fourth time loop run: count=2 -> before we start the next loop, we decrement to 1;
//fifth time loop run: count=1 -> before we start the next loop, we decrement to 0;
//on a sixth "possible" loop, is our condtiion still true? no

//Do While Loops
//Do While loops are similar to while loops that it allows us to repeat actions/tasks as long as the condition is true. However, with a do-while loop, you are able to perform a task at least once even if the condition is not true

// do{
// 	console.log(count);
// 	count--;
// }while (count === 0);
//false agad
//it will run at least one time, before checking

/*while(count===0){
	console.log(count);
	count--;
}
*/
//while loop CHECK BEFORE RUNNING


let counterMini = 20;
while(counterMini !==0){

	console.log("Number: " + counterMini);
	counterMini--;
	
}

let counterMini2 = 20;
while(counterMini2 > 0){ //greater than zero

	console.log("Countdown: " + counterMini2);
	counterMini2--;
	
}



//for loop
//the for loop is a more flexible version of our while and do while loops
//it consists of 3 parts
//1. is the declaration/initialization of the counter
//2. the condition that will be evaluated if the loop will continue
//3. the iteration or the incrementation/decrementation needed to continue and arrive at a terminating/end condition

for(let count = 0; count <=20; count++){
	console.log(count);
}
//initial value ay zero, 
//first loop = count = 0 = before we end the loop -> count = 1
//second loop = count = 1 = before we end the loop -> count = 2
//2nd to the lastloop = count = 19 = before we end the loop -> count =20
//last loop = count = 20 =before the end of the last loop -> count 21
//will we run another loop? Np. Because count no longer meets our condition.


//debug the following for the loop which will show a matrix of a number an addition table of 1-10


for (let x = 1; x <10; x++){
	let sum = 1 + x
	console.log("The sum of " + 1 + " + " + x + " = " + sum);
}

/*for (let m = 1; m <=10; m++){
	let product = 5 * m
	console.log("The product of " + 5 + " x " + m + " = " + product);
}*/

//Continue and Break
/*
	Continue is a keyword that allows the code to go to the next loop without finishing the current code block
*/

for(let counter = 0;counter <=20; counter++){
	//If the current value of count is even
	if(counter % 2 === 0){
		continue;
	}
	console.log(counter);

/*for(let counterE = 0;counterE <=20; counterE++){
	//If the current value of count is even
	if(counterE % 2 !== 0){
		continue;
	}
	//When we use the continue keyword, the code block ff is disregarded and the next loop is run
	//Whenever the value of counter is an even number, we skip to the next loop.
	//Therefore, only odd numbers are shown.
	console.log(counterE);
}*/


/*for(let num = 100; num <=50; num--){

	if(num===100){
		console.log("The number you provided is 100.");
		break;
	}
	else if(num % 10 !==0){
		console.log("The number is divisible by 10. Skipping the number.");
		break;
	}
	else if (num===50){
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}

	console.log(num);
}*/

/*	Break - allows us to end the execution of a loop
	
	*/


if(counter ===1){
	break;
	}
}


/*

loop - print all numbers that are divisible by 5
stop the loop when the loop reaches 100th iteration


*/

for(let counterD = 1;counterD <=100; counterD++){
	if(counterD % 5 !== 0){
		continue;
	}
	console.log(counterD);
}

for(let counterE = 0;counterE <=100; counterE++){
	if(counterE % 5 === 0){
	console.log(counterE);
	}
}